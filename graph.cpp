#include <iostream>
#include <SFML/Graphics.hpp>
#include <vector>

#include "graph.hpp"
#include "algorithm.hpp"
#include "util.hpp"

Graph::Graph(Algorithm* algorithm, sf::Vector2f position, sf::Vector2f size, int maxValue, int minValue)
{
    this->algorithm = algorithm;
    this->position = position;
    this->size = size;

    this->maxValue = maxValue;
    this->minValue = minValue;
}

void Graph::draw(sf::RenderWindow* window)
{
    // draw background
    sf::RectangleShape rect1(this->size);
    rect1.setFillColor(sf::Color::White);
    rect1.setPosition(this->position);
    window->draw(rect1);

    // draw graph outline (real bounds of the graph)
    sf::RectangleShape rect2(sf::Vector2f(this->size.x - this->padding, this->size.y - this->padding));
    rect2.setFillColor(sf::Color::Transparent);
    rect2.setOutlineThickness(this->lineThickness);
    rect2.setOutlineColor(sf::Color::Black);
    rect2.setPosition(sf::Vector2f(this->position.x + (this->padding / 2), this->position.y + (this->padding / 2)));
    window->draw(rect2);

    // draw bars
    std::vector<int> array = this->algorithm->getDisplayVector();
    float barX = rect2.getPosition().x;
    const float barWidth = rect2.getSize().x / (float)array.size();

    for (int i = 0; i < array.size(); i++)
    {
        const float percTotal = (float)array[i] / (float)maxValue;
        const float barHeight = rect2.getSize().y * percTotal;
        float barY = rect2.getPosition().y + (rect2.getSize().y * (1.0 - percTotal));

        sf::RectangleShape bar(sf::Vector2f(barWidth, barHeight));
        bar.setPosition(barX, barY);
        bar.setFillColor(sf::Color::Cyan);
        window->draw(bar);

        barX += barWidth;
    }
}