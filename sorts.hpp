#pragma once

#include <SFML/Graphics.hpp>
#include <vector>

#include "graph.hpp"
#include "algorithm.hpp"

class Sorts
{
    public:
        Sorts();
        void start();
        sf::RenderWindow window;

    private:
        sf::VideoMode desktop;
        std::vector<Algorithm*> algorithms;
        std::vector<Graph*> graphs;
};