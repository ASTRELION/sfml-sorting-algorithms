#include <SFML/Graphics.hpp>

#include "util.hpp"

sf::Vector2f subtract(sf::Vector2f vec, float n)
{
    sf::Vector2f newVec(vec.x - n, vec.y - n);
    return newVec;
}