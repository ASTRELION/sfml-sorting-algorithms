#pragma once

#include <SFML/Graphics.hpp>

class Util
{
    public:
        static sf::Vector2f subtract(sf::Vector2f vec, float n);
};