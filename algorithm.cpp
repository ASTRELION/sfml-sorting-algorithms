#include <vector>
#include <iostream>
#include <thread>
#include <chrono>

#include "algorithm.hpp"

Algorithm::Algorithm(std::vector<int> toSort, long long delay)
{
    this->toSort = toSort;
    this->delay = delay;
    this->sleepTime = 0;
}

Algorithm::~Algorithm()
{

}

void Algorithm::start()
{
    std::thread* sortThread = new std::thread(&Algorithm::sort, this);
}

void Algorithm::sort()
{
    std::cout << "Sorting not implemented" << std::endl;
}

void Algorithm::startTimer()
{
    this->sleepTime = 0;
    this->startTime = std::chrono::high_resolution_clock::now();
}

void Algorithm::endTimer()
{
    auto elapsedTime = std::chrono::high_resolution_clock::now() - this->startTime;

    long long nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(elapsedTime).count() - this->sleepTime;
    long double microseconds = nanoseconds / 1000.0L;
    long double milliseconds = microseconds / 1000.0L;
    long double seconds = milliseconds / 1000.0L;

    std::cout << this->sleepTime << std::endl;
    std::cout << "Sort Took: " << nanoseconds << " nanoseconds" << std::endl;
    std::cout << microseconds << " microseconds" << std::endl;
    std::cout << milliseconds << " milliseconds" << std::endl;
    std::cout << seconds << " seconds" << std::endl;
}

long long Algorithm::highResSleep(long long nanoseconds)
{
    if (nanoseconds <= 0)
    {
        return 0;
    }

    auto now = std::chrono::high_resolution_clock::now();

    long long n = 0;
    while ((n = (std::chrono::high_resolution_clock::now() - now).count()) < nanoseconds);
    this->sleepTime += n;

    return n;
}

void Algorithm::printArray()
{
    for (int i = 0; i < this->toSort.size(); i++)
    {
        std::cout << this->toSort[i] << " ";
    }

    std::cout << std::endl;
}

void Algorithm::setDelay(long long delay)
{
    this->delay = delay;
}

long long Algorithm::getDelay()
{
    return this->delay;
}

std::vector<int> Algorithm::getDisplayVector()
{
    return this->toSort;
}