#include "mergeSort.hpp"

#include <vector>
#include <iostream>

std::vector<int> MergeSort::merge(std::vector<int> left, std::vector<int> right)
{
    std::vector<int> temp(left.size() + right.size());
    int i = 0; // index of left
    int j = 0; // index of right
    int k = 0; // index of new array

    // order between the two arrays
    while (i < left.size() && j < right.size())
    {
        this->highResSleep(this->delay);

        if (left[i] < right[j])
        {
            temp[k++] = left[i++];
        }
        else
        {
            temp[k++] = right[j++];
        }
    }

    // fill whats left of either
    while (i < left.size())
    {
        this->highResSleep(this->delay);
        temp[k++] = left[i++];
    }

    while (j < right.size())
    {
        this->highResSleep(this->delay);
        temp[k++] = right[j++];
    }

    this->dummy = temp;
    return temp;
}

std::vector<int> MergeSort::mergeSort(std::vector<int> vector)
{
    if (vector.size() == 1)
    {
        return vector;
    }

    // middle index of the array
    int middle = vector.size() / 2;

    std::vector<int> left(vector.begin(), vector.begin() + middle);
    std::vector<int> right(vector.begin() + middle, vector.begin() + vector.size());

    left = this->mergeSort(left); // sort the left half
    right = this->mergeSort(right); // sort the right half
    return this->merge(left, right); // merge the two halves
}

void MergeSort::sort()
{
    this->startTimer();

    this->dummy = this->toSort;
    mergeSort(this->toSort);

    this->endTimer();
}

std::vector<int> MergeSort::getDisplayVector()
{
    return this->dummy;
}