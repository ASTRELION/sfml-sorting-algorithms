#include <iostream>

#include "bubbleSort.hpp"

void BubbleSort::sort()
{
    std::cout << "Sorting with Bubble Sort" << std::endl;

    this->startTimer();

    for (int i = 0; i < this->toSort.size() - 1; i++)
    {
        for (int j = 0; j < this->toSort.size() - i - 1; j++)
        {
            if (this->toSort[j] > this->toSort[j + 1])
            {
                this->highResSleep(this->delay);

                int temp = this->toSort[j];
                this->toSort[j] = this->toSort[j + 1];
                this->toSort[j + 1] = temp;
            }
        }
    }

    this->endTimer();
}