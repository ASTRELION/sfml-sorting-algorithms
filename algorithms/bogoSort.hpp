#pragma once

#include "../algorithm.hpp"

class BogoSort : public Algorithm
{
    public:
        BogoSort(std::vector<int> toSort) : Algorithm(toSort) {};

        void sort();
        bool isSorted();
};