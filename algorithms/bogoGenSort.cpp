#include <iostream>
#include <random>
#include <vector>
#include <list>
#include <iterator>

#include "bogoGenSort.hpp"

void BogoGenSort::sort()
{
    std::cout << "Sorting with Bogo Sort" << std::endl;

    this->startTimer();

    std::list<int> arrayAsList;

    while (!isSorted())
    {
        std::copy(this->toSort.begin(), this->toSort.end(), std::back_inserter(arrayAsList));

        for (int i = 0; i < this->toSort.size(); i++)
        {
            this->highResSleep(this->delay);

            int j = rand() % arrayAsList.size();
            auto front = arrayAsList.begin();
            std::advance(front, j);
            this->toSort[i] = *front;
            arrayAsList.erase(front);
        }
    }

    this->endTimer();
}

bool BogoGenSort::isSorted()
{
    for (int i = 1; i < this->toSort.size(); i++)
    {
        if (this->toSort[i] < this->toSort[i - 1])
        {
            return false;
        }
    }

    return true;
}