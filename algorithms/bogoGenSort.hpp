#pragma once

#include "../algorithm.hpp"

class BogoGenSort : public Algorithm
{
    public:
        BogoGenSort(std::vector<int> toSort) : Algorithm(toSort) {};

        void sort();
        bool isSorted();
};