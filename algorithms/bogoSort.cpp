#include <iostream>
#include <random>

#include "bogoSort.hpp"

void BogoSort::sort()
{
    std::cout << "Sorting with Bogo Sort" << std::endl;

    this->startTimer();

    while (!isSorted())
    {
        int i = rand() % this->toSort.size();
        int j = rand() % this->toSort.size();

        this->highResSleep(this->delay);

        int temp = this->toSort[i];
        this->toSort[i] = this->toSort[j];
        this->toSort[j] = temp;
    }

    this->endTimer();
}

bool BogoSort::isSorted()
{
    for (int i = 1; i < this->toSort.size(); i++)
    {
        if (this->toSort[i] < this->toSort[i - 1])
        {
            return false;
        }
    }

    return true;
}