#pragma once

#include <vector>

#include "../algorithm.hpp"

class InsertionSort : public Algorithm
{
    public:
        // O(n^2)
        InsertionSort(std::vector<int> toSort) : Algorithm(toSort) {};

        void sort();
};