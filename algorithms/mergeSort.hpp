#pragma once

#include <vector>

#include "../algorithm.hpp"

class MergeSort : public Algorithm
{
    public:
        MergeSort(std::vector<int> toSort) : Algorithm(toSort) {};

        void sort();
        std::vector<int> merge(std::vector<int> left, std::vector<int> right);
        std::vector<int> mergeSort(std::vector<int> vector);

        std::vector<int> getDisplayVector();

    private:
        std::vector<int> dummy;
};