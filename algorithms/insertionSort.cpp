#include <iostream>
#include <vector>

#include "insertionSort.hpp"

void InsertionSort::sort()
{
    std::cout << "Sorting with Insertion Sort" << std::endl;

    this->startTimer();

    for (int i = 1; i < this->toSort.size(); i++)
    {
        int current = this->toSort[i];

        int j = i - 1;
        while (current < this->toSort[j] && j >= 0)
        {
            this->highResSleep(this->delay);

            int temp = this->toSort[j];
            this->toSort[j] = current;
            this->toSort[j + 1] = temp;
            j--;
        }
    }
    
    this->endTimer();
}