#pragma once

#include <vector>

#include "../algorithm.hpp"

class BubbleSort : public Algorithm
{
    public:
        BubbleSort(std::vector<int> toSort) : Algorithm(toSort) {};

        void sort();
};