# SFML Sorting
This is a simple [SFML](https://www.sfml-dev.org/) desktop app for visualizing different sorting algorithms.

Algorithms are roughly sorted from quickest to slowest.

## Algorithms

*Ordered alphabetically*

### [Bogo Sort](algorithms/bogoSort.cpp)

There's 2 implementations of Bogo sort in the application. Bogo sort just sorts at random and checks if its ordered.

1. Generates 2 random numbers and swaps the corresponding indices in the array
2. Generates a completely random and new ordering of the entire array

**Time Complexity**: O(∞)

### [Bubble Sort](algorithms/bubbleSort.cpp)

**Time Complexity**: O(n^2)

### [Insertion Sort](algorithms/insertionSort.cpp)

**Time Complexity**: O(n^2)

### [Merge Sort](algorithms/mergeSort.cpp)

Recursively splits the array in half, and runs merge sort on the new halves. Combines both halves with an auxillary merge function.

**Time Complexity**: O(n * log(n))

## Compile & Run

You'll need to have [SFML installed](https://www.sfml-dev.org/tutorials/2.5/start-linux.php) as well as at least C++11.

1. Download or clone the repository
2. `cd` into the repository folder
3. Run `make`
4. Run `./sorts-app` to run the compiled application