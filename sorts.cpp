#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <ctime>

#include "sorts.hpp"
#include "graph.hpp"
#include "algorithms/insertionSort.hpp"
#include "algorithms/bogoSort.hpp"
#include "algorithms/bogoGenSort.hpp"
#include "algorithms/bubbleSort.hpp"
#include "algorithms/mergeSort.hpp"

Sorts::Sorts()
{
    desktop = sf::VideoMode::getDesktopMode();
    window.create(
        sf::VideoMode(desktop.width * (4.f / 5.f), desktop.height * (1.f / 2.f)), 
        "Sorting Algorithms"
    );
    window.setVerticalSyncEnabled(true);
    window.setPosition(sf::Vector2i(
        (desktop.width / 2) - (window.getSize().x / 2), 
        (desktop.height / 2) - (window.getSize().y / 2)
    ));

    // create array
    std::vector<int> toSort(10000);
    const int maxValue = 10000;
    const int minValue = 1;
    std::srand(std::time(nullptr));

    std::cout << "Array to sort: ";

    for (int i = 0; i < toSort.size(); i++)
    {
        
        toSort[i] = (rand() % (maxValue - minValue + 1)) + minValue;
        std::cout << toSort[i] << " ";
    }

    std::cout << std::endl;

    // initialize algorithms
    this->algorithms.push_back(new MergeSort(toSort));
    this->algorithms.push_back(new InsertionSort(toSort));
    this->algorithms.push_back(new BubbleSort(toSort));
    this->algorithms.push_back(new BogoGenSort(toSort));
    this->algorithms.push_back(new BogoSort(toSort));

    // initialize graphs
    const int borderWidth = 25;
    const float rectSize = std::min(
        (float)this->window.getSize().y / 2.f, 
        (float)(this->window.getSize().x - (borderWidth * (this->algorithms.size() - 1))) / ((float)this->algorithms.size() + 1)
    );

    int xPos = std::max(
        0.f, 
        (this->window.getSize().x / 2) - ((this->algorithms.size()) * (rectSize / 2)) - ((borderWidth * (this->algorithms.size() - 1)) / 2)
    );
    int yPos = this->window.getSize().y - std::min((int)xPos, (int)rectSize / 2) - rectSize;

    for (Algorithm* a : this->algorithms)
    {
        this->graphs.push_back(new Graph(a, sf::Vector2f(xPos, yPos), sf::Vector2f(rectSize, rectSize), maxValue, minValue));
        xPos += rectSize + borderWidth;
    }

    for (Algorithm* a : this->algorithms)
    {
        a->setDelay(100000);
        a->start();
    }
}

void Sorts::start()
{
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        window.clear();

        for (Graph* graph : this->graphs)
        {
            graph->draw(&this->window);
        }

        window.display();
    }
}