#pragma once

#include <vector>
#include <chrono>

class Algorithm
{
    public:
        // Represents a sorting algorthm; default delay of 1ms after array write operations
        Algorithm(std::vector<int> toSort, long long delay = 1 * 1000 * 1000);
        virtual ~Algorithm();

        // Creates thread and starts sort
        void start();
        // Sorts toSort with the defined sorting algorithm
        virtual void sort();
        void startTimer();
        void endTimer();
        // Dirty spin-lock sleep
        long long highResSleep(long long nanoseconds);
        // Prints the current space-delimited toSort array
        void printArray();

        long long getDelay();
        void setDelay(long long delay);

        virtual std::vector<int> getDisplayVector();

        // The array to sort
        std::vector<int> toSort;

    protected:
        // delay in nanoseconds
        long long delay;
        // Time spent sleeping
        long long sleepTime;

    private:
        std::chrono::_V2::system_clock::time_point startTime;
};