#pragma once

#include <SFML/Graphics.hpp>

#include "algorithm.hpp"

class Graph
{
    public:
        Graph(Algorithm* algorithm, sf::Vector2f position, sf::Vector2f size, int maxValue = 1000, int minValue = 1);
        void draw(sf::RenderWindow* window);

    private:
        Algorithm* algorithm;
        sf::Vector2f position;
        sf::Vector2f size;

        // Total x/y padding (e.g. 20 is actually 10 padding in traditional CSS sense)
        const float padding = 10 * 2;
        const float lineThickness = 2;

        // Maximum value of any single array integer (used for displaying bar graph)
        int maxValue;
        // Minimum value of any single array integer
        int minValue;
};